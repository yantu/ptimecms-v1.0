<div class="widthAuto">
	<div  class="container_main">
	<p>{{ setting.site_title }} <span>Copyright 2015-2020</span></p>
	友情链接：
	{% if favolink %}
		{% for link in favolink %}
		    {% if  loop.last %}
			  <a href="{{ link.url }}" target="_blank">{{ link.title }}</a>
			  {% else %}
			  <a href="{{ link.url }}" target="_blank">{{ link.title }}</a>|
			  {% endif %}
		{% endfor %}
	{% endif %}
	<br>
	地址：{{ setting.address }} 邮编：{{ setting.post_code }}
	<br>
	备案号：{{ setting.icp_number }}
	</div>
	<!--[if !IE]><!-->
		<div id="qrcode" class="pure-hidden-phone"></div>
	<!--<![endif]-->
	<!--[if IE]>
		<a href="/" class="logo">{{ image("img/logo.png") }}</a>
	<![endif]-->
</div>