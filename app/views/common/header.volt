<div id="topBarDesktop" class="warterNav_desktop">
    <div class="widthAuto">
        <span class="left">
            <a href="/">{{ image("img/logo.png") }}</a>
        </span>
        {{ form("/search", "method": "get","class":"right pure-form") }}
            {{ text_field("content","type":"text","class":"left")}}
            {{ submit_button("","class":"left")}}  
        {{ endform() }}
    </div>
</div>
<div id="topBarPhone" class="warterNav_Phone">
    <a href="/" class="left">{{ image("img/logo.png") }}</a>
    <a href="#" id="menuIcon" data-rel="more" ></a>
</div>
<nav id="more">
        {{ form("/search", "method": "get","class":"search_phone") }}
            {{ text_field("content","type":"text")}}
            {{ submit_button("","class":"")}}  
        {{ endform() }}
    <ul  class=" navLevel1 widthAuto">
        {% for link in nav[0] %}
            <li>
                {% if nav[link.id] is defined %}
                <span class="menu-switch">{{ link.title }}</span>
                <em class="menu-switch-arrow warterNav_Phone"></em>
                <ul class="navLevel2">
                {% for linkTwo in nav[link.id] %}
                    <li>
                        <a href="{{ linkTwo.url }}">{{ linkTwo.title }}</a>
                    </li>
                {% endfor %}
                </ul>
                {% else %}
                <a href="{{ link.url }}">{{ link.title }}</a>
                {% endif %}  
            </li>
        {% endfor %}
        <li>
            <a href="http://www.qhdtdqy.com:8000/competition">在线报名</a>
            <em class="menu-switch-arrow warterNav_Phone"></em>
        </li>
    </ul>
</nav>     
