var min_height = $(window).height()-$("#header").height()-$("#footer").height();
$("#container").css("min-height",min_height);
// ie7-不支持@media的补丁
if (!Modernizr.mq('only all')) {
    var screen_width = $(window).width();
    if (screen_width>1200) {
        $(".widthAuto").css("width","1200px");
    }else if(screen_width<1200){
        console.log(screen_width);
        $(".widthAuto").css("width","980px");
    }
    $(window).resize(function() {
        var screen_width = $(window).width();
        if (screen_width>1200) {
            $(".widthAuto").width(1200);
        }else if(screen_width<1200){
            $(".widthAuto").width(980);
        }
    });
}

// ie7-的placeholder不支持补丁
var JPlaceHolder = {
    //检测
    _check : function(){
        return 'placeholder' in document.createElement('input');
    },
    //初始化
    init : function(){
        if(!this._check()){
            this.fix();
        }
    },
    //修复
    fix : function(){
        jQuery(':input[placeholder]').each(function(index, element) {
            var self = $(this), txt = self.attr('placeholder');
            self.wrap($('<div></div>').css({position:'relative', zoom:'1', border:'none', background:'none', padding:'none', margin:'none'}));
            var pos = self.position(), h = self.outerHeight(true), paddingleft = self.css('padding-left');
            var holder = $('<span></span>').text(txt).css({position:'absolute', left:pos.left, top:pos.top, height:h, lienHeight:h, paddingLeft:paddingleft, color:'#aaa'}).appendTo(self.parent());
            self.focusin(function(e) {
                holder.hide();
            }).focusout(function(e) {
                if(!self.val()){
                    holder.show();
                }
            });
            holder.click(function(e) {
                holder.hide();
                self.focus();
            });
        });
    }
};
jQuery(function(){
    JPlaceHolder.init();    
});

// 二维码
function qcodetochar(str){  
    var out, i, len, c;  
    out = "";  
    len = str.length;  
    for (i = 0; i < len; i++) {  
        c = str.charCodeAt(i);  
        if ((c >= 0x0001) && (c <= 0x007F)) {  
            out += str.charAt(i);  
        } else if (c > 0x07FF) {  
            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));  
            out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));  
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));  
        } else {  
            out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));  
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));  
        }  
    }  
    return out;  
};  
// 生成二维码
$.fn.qcode = function(options){  
    if(options){  
        var opt = {};  
        if(typeof options == 'string'){  
            opt.text = options;  
        }else{  
            if(options.text) opt.text = options.text;  
            if(options.type && options.type == 'ch') opt.text = qcodetochar(opt.text);  
            if(options.render && options.render == 'table') opt.render = options.render;  
            if(options.width) opt.width = options.width;  
            if(options.height) opt.height = options.height;  
        }  
  
        $(this).qrcode(opt);  
    }  
};  
var url = location.href;
$('#qrcode').qcode({  
    text : url,  
    type : 'cn',  
    width : 100,  
    height : 100  
});

// 大图轮播
var mySwiper1 = new Swiper('.swiper-container1', {
    autoplay: 5000,
    loop: true,
    calculateHeight : true,
    keyboardControl : true,
    pagination: '.pagination',
    paginationClickable :true
});
$('#btn1').click(function(){
mySwiper1.swipePrev(); 
})
$('#btn2').click(function(){
mySwiper1.swipeNext(); 
})
var mySwiper2 = new Swiper('.swiper-container2', {
    autoplay: 5000,
    loop: true
})

// 在线留言
$("#submit").click(function(){
    console.log($("#content").val());
	var str = {"content":$("#message").val(),"name":$("#name").val(),"email":$("#email").val()};
	str=JSON.stringify(str);
	if ($("#message").val()!= "" && $("#name").val()!= "" && $("#email").val()!= "") {
        var search_str = /^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/;
        var email_val = $("#email").val();
        if(search_str.test(email_val)){
            $.ajax({  
                type: 'POST',  
                url: 'topic/1/comment?content='+str+'&father_id=0',  
                success: function(data){  
                    if (!data.error) {
                        alert("谢谢留言");
                        $("#message").val('');
                        $("#name").val('');
                        $("#email").val('');
                    };
                },  
                error: function(data){  
                    console.log(str);
                    alert("留言失败");
                }  
            });
        }else{
          alert("email格式不正确");
          $("#email").val('');
          $("#email").focus();
        }
	}else{
		alert("信息不完整");
	};
	return false;
})
// $("#cancel").click(function(){
// 	$("#content").val('');
// 	$("#name").val('');
// 	$("#email").val('');
// })

// 文章列表页
if($("#hot_news").length>0){
    $(".model_list_word ul li:first-child").hide();
}else{
    $(".model_list_word ul li:first-child").show();
};

var page = 2;
function getMore(category_id,page_name,type){
    $('.page_more').hide();
    $('.img_loading').show();
    page_name = page_name;
    category_id = category_id;
    if (type) {
        var url = '/'+page_name+'/more?page='+page+'&category_id='+category_id
    }else{
         var url =  '/'+category_id+'/more?page='+page+'&content='+page_name;
    }
    $.ajax({  
        type: 'GET',
        url: url,
        success: function(data){
            $(data.data).each(function(){
                title = $(this)[0]['title'];
                created_at = $(this)[0]['created_at'];
                id = $(this)[0]['id'];
                img_dir = $(this)[0]['img_dir'];
                link_url = $(this)[0]['url'];
                if ( (page_name == "article" && type == 1) || (category_id == "article" && type == 0)) {
                    var tpl =   '<li>\
                                    <span class="article_info">\
                                    <a href=../article/'+id+'>'+title+'</a>\
                                    </span>\
                                    <span class="article_time">'+created_at+'</span>\
                                </li>';
                }else if((page_name == "album" && type == 1) || (category_id == "album" && type == 0)){
                    console.log("#page_more_"+category_id);
                    var tpl =   '<div class="pure-u-1-3">\
                                    <div class="album album_box">\
                                        <a href=../album/'+id+'><img src='+img_dir+'></a>\
                                        <div class="album_info">\
                                            <h4>'+title+'</h4>\
                                        </div>\
                                    </div>\
                                </div>';
                }else if ((page_name == "link" && type == 1) || (category_id == "link" && type == 0)) {
                    var tpl =   '<li>\
                                    <a href='+link_url+'>'+title+'</a>\
                                </li>';
                }else{
                    var tpl =   '<li>\
                                    <a href="../topic/'+id+'">'+title+'</a>\
                                </li>';
                };
                $("#page_more_"+category_id).append(tpl);
                $('.img_loading').hide();
            });
            if (data.data.length!=15) {
                $("#"+category_id).css("display","none");
            };
            page+=1;
        },  
        error: function(data){  
            console.log("出错了");
        }  
    });
}