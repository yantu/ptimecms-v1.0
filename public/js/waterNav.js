$(function(){
	FastClick.attach(document.body);
	if ($(window).width()>500) {
		var num = $("nav>ul>li").length;
		var nav_width = $("nav>ul").width();
		var li_width = nav_width/num;
		$("nav>ul>li").css({"width":li_width+"px"});
	}else{
		if(!+'\v1' && !'1'[0]){ 
			$("nav>ul>li").css({"min-width":"100px"});
		}else{
			$("nav>ul>li").css({"width":"100%"});
		}
	};
	var display = false;
	$("#menuIcon,nav a").click(function(){
		$("#more").css("min-height",$(window).height());
		if ($("#menuIcon").is(":visible")) {
			if ($(this).attr('id') == "menuIcon") {
				$("em").removeClass("rotate");
				$(".navLevel2").hide();
			}
			display = !display;
			$("#more").animate({
				"left":(display?"0px":"-250px")
			},500)
			
		};
	});

	$(".menu-switch-arrow,.menu-switch").click(function(){
		$(this).siblings("ul").toggle();
		if ($(this).siblings("ul").is(":visible")) {
			if ($(this).hasClass('menu-switch-arrow')) {
				$(this).addClass("rotate");
			}else{
				$(this).next().addClass("rotate");
			};
			$(this).parent().siblings().children("em").removeClass("rotate");
		}else{
			if ($(this).hasClass('menu-switch-arrow')) {
				$(this).removeClass("rotate");
			}else{
				$(this).next().removeClass("rotate");
			};
		}; 
		$(this).parent().siblings().children("ul").hide();	
		return false;	
	})
	$( window ).resize(function() {
		if ($(window).width()>500) {
			var num = $("nav>ul>li").length;
			var nav_width = $("nav>ul").width();
			var li_width = nav_width/num;
			$("nav>ul>li").css({"width":li_width+"px"});
		}else{
			if(!+'\v1' && !'1'[0]){ 
				$("nav>ul>li").css({"min-width":"100px"});
			}else{
				$("nav>ul>li").css({"width":"100%"});
			}
		};
	});
	if ($(window).width()>700) {
		$("nav li").mouseover(function(){
			$(this).css("background","#888");
			return false;
		})
		$("nav li").mouseout(function(){
			$(this).css("background","#4d4d4d");
		})
	};
		
})